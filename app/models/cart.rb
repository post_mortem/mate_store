class Cart < Ohm::Model
  attribute :subtotal
  list :products, Product
end
