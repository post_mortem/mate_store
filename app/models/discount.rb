class Discount < Ohm::Model
  attribute :rebate
  attribute :product_amount

  reference :product, Product
end
