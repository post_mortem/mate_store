class Product < Ohm::Model
  attribute :name
  attribute :description
  attribute :price

  set :discounts, Discount

  def validate
    assert_presense :name
  end
end
