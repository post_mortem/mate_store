class DiscountsController < ApplicationController
  before_action :set_discount, only: [:show, :edit, :update, :destroy]

  def index
    @discounts = Discount.all.to_a
  end

  def show
  end

  def new
    @discount = Discount.new
  end

  def edit
  end

  def create
    @discount = Discount.new(discount_params)

    if @discount.save
      product = Product[params[:product]]
      product&.discounts&.add @discount

      redirect_to discounts_path, notice: 'Discount was successfully created.'
    else
      render :new
    end
  end

  def update
    if @discount.update(discount_params)
      redirect_to discounts_path, notice: 'Discount was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /discounts/1
  def destroy
    @discount.delete
    redirect_to discounts_url, notice: 'Discount was successfully destroyed.'
  end

  private
    def set_discount
      @discount = Discount[params[:id]]
    end

    def discount_params
      { product_amount: params[:product_amount], rebate: params[:rebate], product: Product[params[:product]&.to_i] }
    end
end
