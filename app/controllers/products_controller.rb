class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :edit, :update, :destroy]
  before_action :set_cart, only: [:index, :add_to_cart, :remove_from_cart]

  def index
    @products = Product.all.to_a

    selected = Product.fetch(@cart.products.ids)
    uniq_selected = Product.fetch(Set.new(@cart.products.ids).to_a)

    @selected_products = uniq_selected.reduce([]) do |res, el|
      amount, price = calculate_discount.(el)
      res << { product: el, amount: amount, price: price }
    end

    @subtotal = selected.reduce(0) do |res, el|
      _, price, _ = calculate_discount.(el)
      res += price
    end
     p @cart.products.size

    @discount = selected.reduce(0) do |res, el|
      _, _, discount = calculate_discount.(el)
      res += discount.round
    end.to_f
  end

  def add_to_cart
    respond_to do |format|
      if @cart.products.push(Product[params[:id]])
        format.html { redirect_to products_path, notice: 'Product was successfully removed from cart' }
      end
    end
  end

  def remove_from_cart
    respond_to do |format|
      if @cart.products.delete(Product[params[:id]])
        format.html { redirect_to products_path, notice: 'Product was successfully added' }
      end
    end
  end


  def show
  end

  def new
    @product = Product.new
  end

  def edit
  end

  def create
    @product = Product.new(product_params)

    respond_to do |format|
      if @product.save
        format.html { redirect_to products_path, notice: 'Product was successfully created' }
        format.json { render :show, status: :created, location: @product }
      else
        format.html { render :new }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @product.update(product_params)
        format.html { redirect_to product_path, notice: 'Product was successfully updated' }
        format.json { render :show, status: :ok, location: @product }
      else
        format.html { render :edit }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @product.delete
    respond_to do |format|
      format.html { redirect_to products_url, notice: 'Product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_product
      @product = Product[params[:id]]
    end

    def calculate_discount
      lambda do |el|
        discount = Discount.fetch(el.discounts.ids)&.first
        discount_amount = discount&.product_amount.to_i
        amount = @cart.products.
                 ids.count(el.id)

        discount_part = if discount && amount >= discount_amount
                          (discount&.rebate.to_f / 100) * el.price.to_i
                        else
                          0
                        end

        price = if discount && amount >= discount_amount
                  el.price.to_f - discount_part.round
                else
                  el.price.to_f
                end
        [amount, price, discount_part]
      end
    end

    def set_cart
      @cart = Cart[1] || Cart.new.save
    end

    def product_params
      { name: params[:name], price: params[:price], description: params[:description] }
    end
end
