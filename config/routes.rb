Rails.application.routes.draw do
  root to: 'products#index'
  resources :discounts, except: :update do
    member do
      post 'update', as: 'post_update'
    end
  end
  resources :products, except: :update do
    member do
      post 'update', as: 'post_update'
      put 'add_to_cart'
      put 'remove_from_cart'
    end
  end
end
