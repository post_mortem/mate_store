require 'rails_helper'

RSpec.describe Product, type: :model do
  after :each do
    Ohm.flush
  end

  it "has all and save methods" do
    expect(described_class).to respond_to :all
    expect(Product.new).to respond_to :save
  end

  it 'can be queried by id' do
    product = Product.create(name: 'Mate', price: '45')
    res = Product[product.id]

    expect(res.name).to eq product.name
    expect(res.price).to eq product.price
  end
end
