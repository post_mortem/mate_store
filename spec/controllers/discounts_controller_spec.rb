require 'rails_helper'

RSpec.describe DiscountsController, type: :controller do

  let(:product_id) { Product.new({ name: 'yerba mate', price: 26 }).save }
  let(:valid_attributes) {
    { product_amount: 2, rebate: 35, product: product_id }
  }

  describe "GET #index" do
    it "assigns all discounts as @discounts" do
      discount = Discount.create valid_attributes
      get :index, {}
      expect(assigns(:discounts)).to eq([discount])
    end
  end

  describe "GET #edit" do
    it "assigns the requested discount as @discount" do
      discount = Discount.create valid_attributes
      get :edit, {:id => discount.id}
      expect(assigns(:discount)).to eq(discount)
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Discount" do
        expect {
          post :create, valid_attributes
        }.to change(Discount.all, :size).by(1)
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        { product_amount: 2, rebate: 67, product: Product.new.save }
      }

      it "assigns the requested discount as @discount" do
        discount = Discount.create valid_attributes
        put :update, {:id => discount.id}.merge(valid_attributes)
        expect(assigns(:discount)).to eq(discount)
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested discount" do
      discount = Discount.create valid_attributes
      expect {
        delete :destroy, {:id => discount.id}
      }.to change(Discount.all, :size).by(-1)
    end
  end
end
